<h1>Dungeon World Master Companion</h1>

This is a project for an application to help Dungeon World's Game Masters with some tasks.

Currently it holds only the mobile app project, but there's an intention to build a web version on google engine or similar free hosting service.
<br /><br />
<h2>Mobile App</h2>

The Mobile app project of DWMasterCompanion was made using <b>Ionic Framework Version 2.</b>
<br /><br />
<h3>Building the mobile app</h3>

To configure the building setup, build and run de mobile app, follow the instructions below:

<ol>
    <li>Download and install Node.js: https://nodejs.org/en/</li>
    <li>
        Install Ionic by executing the following command: <br /> 
        <code>npm install -g cordova ionic</code>
    </li>
    <li>
        Download the project from GitLab: <br />
        <code>https://gitlab.com/danielsousat/dwmastercompanion.git</code>
    </li>
    <li>
        Access the mobile project folder <i>dwmastercompanion/mobile/DWMasterCompanion</i> and install the dependencies: <br />
        <code>npm install</code>
    </li>
    <li>
        To test the app in a browser, run the following command: <br />
        <code>ionic serve</code>
    </li>
</ol>

In order to compile and run the app on Android or iOS, follow the instructions on ionic's website: http://ionicframework.com/docs/v2/setup/deploying/ .
<br />
<p><b>PS: Fix to common problems:</b></p>

<ul>
    <li>
        If the app splashscreen is not working, or the app is showing the Cordova's Default Splashscreen: just install Cordova's Splashscreen Plugin using the command below: <br />
        <code>cordova plugin add cordova-plugin-splashscreen --save</code>
    </li>
    <li> If it still doesn't work, run the commands below: <br />
        <code>ionic platform rm android</code><br />
        <code>ionic platform add android@latest</code><br />
        <code>ionic resources</code>
    
    </li>
</ul>

<br /><br />

<h3>Building the app for production</h3>
<p>Building the app in production mode will render a comprised code and a faster app. To do so for Android, just run the command below:</p>
<code>ionic run android --prod --device</code>

<br /><br />

<h3>Changing the app's icon and splash screen</h3>

In order to change the app's icon and splash screen, follow the steps below:

<ol>
    <li>
        <p>Substitute the <i>icon.png</i> and <i>splash.png</i> files that are on the folder <i>dwmastercompanion/mobile/DWMasterCompanion/resources</i></p>
        <p><b>Important:</b> the icon image must be 192 x 192 pixel sized and the splash image must be 2208 x 20208 px sized.</p>
        
    </li>
    <li>
        Run the following command:<br />
        <code>ionic resources</code>
    </li>
</ol>

After that, you will see that the icon and splash many files were substituted on all folders on <i>dwmastercompanion/mobile/DWMasterCompanion/resources</i>.
<br /><br />
<h3>Updating the project's dependencies</h3>

To update all of the project dependencies, use the following tool: https://www.npmjs.com/package/npm-check-updates.

To do so, follow the steps below:

<ol>
    <li>
        Install the tool: <code>npm install -g npm-check-updates</code>
    </li>
    <li>Run: <code>ncu -u</code></li>
    <li>Run: <code>npm update</code></li>
</ol>