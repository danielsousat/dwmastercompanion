import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base-page';

/*
  Generated class for the ModifiersTable page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-modifiers-table',
  templateUrl: 'modifiers-table.html'
})
export class ModifiersTablePage extends BasePage {

  static titleCode: string = "MODIFIERS_TABLE_TITLE";

  table = {
    header: ["VALUE", "MODIFIER"],
    rows: [
        ["1 - 3", "-3"],
        ["4 - 5", "-2"],
        ["6 - 8", "-1"],
        ["9 - 12", "0"],
        ["13 - 15", "+1"],
        ["16 - 17", "+2"],
        ["18", "+3"]
      ]
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    super();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModifiersTablePage');
  }

  getTitleCode(): string {

    return ModifiersTablePage.titleCode;
  }

}
