import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BasePage } from '../base-page';

/*
  Generated class for the About page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage extends BasePage {

  static titleCode: string = 'ABOUT_TITLE';

  constructor(public navCtrl: NavController) {

    super();
  }

  ionViewDidLoad() {
    console.log('Hello AboutPage Page');
  }

  getTitleCode(): string {

    return AboutPage.titleCode;
  }

}
