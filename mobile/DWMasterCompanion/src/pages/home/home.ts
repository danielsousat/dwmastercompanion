import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BasePage } from '../base-page';
import { PagesService } from '../../providers/pages-service';

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends BasePage {

  static titleCode: string = 'HOME_TITLE';

  constructor(public navCtrl: NavController, public pagesService: PagesService) {
    super();
  }

  ionViewDidLoad() {
    console.log('Hello HomePage Page');
  }

  getTitleCode(): string {

    return HomePage.titleCode;
  }

  getPages() {

     return this.pagesService.getPagesWithoutHomeAndAbout();
  }

  openPage(page) {

    this.navCtrl.push(page.component);
  }

}
