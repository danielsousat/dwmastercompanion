import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BasePage } from '../base-page';
import { NpcGeneratorService } from '../../providers/npc-generator-service';
import { Npc } from '../../dto/npc';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CustomValidators } from '../../validators/custom-validators';
import { TranslateUtil } from '../../providers/translate-util';

/*
  Generated class for the NpcGenerator page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-npc-generator',
  templateUrl: 'npc-generator.html'
})
export class NpcGeneratorPage extends BasePage {

  static titleCode: string =  'NPC_GENERATOR_TITLE'; 
  npc: Npc;
  npcForm: FormGroup;
  numberOfPersonalities: FormControl;
  

  constructor(public navCtrl: NavController, public generatorService : NpcGeneratorService,
    public formBuilder: FormBuilder, public translateUtil: TranslateUtil) {

    super();

    this.npc = this.generatorService.lastGenerated;

    this.configureForm();    
  }

  private configureForm() {

    this.numberOfPersonalities = new FormControl(this.generatorService.lastNumberOfPersonalities,
      [
        Validators.required,
        CustomValidators.minValue(NpcGeneratorService.NUM_MIN_PERSONALITIES),
        CustomValidators.maxValue(NpcGeneratorService.NUM_MAX_PERSONALITIES)
      ]);

    this.npcForm = this.formBuilder.group({
      "numberOfPersonalities": this.numberOfPersonalities
    });
  }

  ionViewDidLoad() {
    console.log('Hello NpcGeneratorPage Page');
  }

  getTitleCode(): string {

    return NpcGeneratorPage.titleCode;
  }

  generateNpc() {
    
    try {
      this.npc = this.generatorService.generateNpc(this.numberOfPersonalities.value);  
    } catch (error) {
      
      console.error(this.translateUtil.getTranslatedErrorMessage(error));
      console.error(error);
    }
     
  }

  getErrorMessage(field: string) {

    return this.generatorService.getErrorMessage(field);
  }
}
