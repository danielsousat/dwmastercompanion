import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base-page';

/*
  Generated class for the DamageRules page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-damage-rules',
  templateUrl: 'damage-rules.html'
})
export class DamageRulesPage extends BasePage {

  static titleCode: string = "DAMAGE_RULES_TITLE";

  generalRules = [
    {header: 'DAMAGE.FROM_MULTIPLE.HEADER', description: 'DAMAGE.FROM_MULTIPLE.DESCRIPTION'},
    {header: 'DAMAGE.TO_MULTIPLE.HEADER', description: 'DAMAGE.TO_MULTIPLE.DESCRIPTION'},
    {header: 'DAMAGE.STUNNING.HEADER', description: 'DAMAGE.STUNNING.DESCRIPTION'},
    {header: 'DAMAGE.BEST_ROLL.HEADER', description: 'DAMAGE.BEST_ROLL.DESCRIPTION'},
    {header: 'DAMAGE.WORST_ROLL.HEADER', description: 'DAMAGE.WORST_ROLL.DESCRIPTION'}
  ];

  otherDamageRules: string[] = [
    'DAMAGE.OTHER.0',
    'DAMAGE.OTHER.1',
    'DAMAGE.OTHER.2',
    'DAMAGE.OTHER.3'
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    super();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DamageRulesPage');
  }

  getTitleCode(): string {

    return DamageRulesPage.titleCode;
  }
}
