import { Component, ViewChild } from '@angular/core';
import { NavController, Content } from 'ionic-angular';
import { BaseFilterPage } from '../base-filter-page';
import { LabelsService } from '../../providers/labels-service';
import { Label } from '../../dto/label';
import { LabelFilter } from '../../dto/label-filter';

/*
  Generated class for the Labels page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-labels',
  templateUrl: 'labels.html'
})
export class LabelsPage extends BaseFilterPage<Label, LabelFilter> {

  @ViewChild(Content) content: Content;
  
  static titleCode: string = "LABELS_TITLE";
  
  constructor(public navCtrl: NavController, public labelsService: LabelsService) {

    super(labelsService); 
  }

  ionViewDidLoad() {
    console.log('Hello LabelsPage Page');
  }

  getTitleCode(): string {

    return LabelsPage.titleCode;
  }
}
