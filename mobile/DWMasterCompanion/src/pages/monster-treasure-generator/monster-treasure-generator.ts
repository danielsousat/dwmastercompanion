import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MonsterTreasureGeneratorService } from '../../providers/monster-treasure-generator-service';
import { BasePage } from '../base-page';
import { Die } from '../../dto/die';
import { MonsterTreasureInputData, MonsterOption } from '../../dto/monster-treasure-input-data';
import { MonsterTreasure } from '../../dto/monster-treasure';

/*
  Generated class for the MonsterTreasureGenerator page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-monster-treasure-generator',
  templateUrl: 'monster-treasure-generator.html'
})
export class MonsterTreasureGeneratorPage extends BasePage {

  static titleCode: string = "MONSTER_TREASURE_GENERATOR_TITLE";

  monsterTreasureInputData: MonsterTreasureInputData;
  monsterTreasures: Array<MonsterTreasure>;
  selectedMonsterOptions: number[];
  selectedMonsterDie: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public generatorService: MonsterTreasureGeneratorService) {
      super();

      this.monsterTreasureInputData = generatorService.lastMonsterTreasureInputData;
      this.monsterTreasures = generatorService.lastMonsterTreasuresGenerated;
      this.selectedMonsterOptions = this.monsterTreasureInputData.monsterOptionsToTypeArray();
      this.selectedMonsterDie = this.monsterTreasureInputData.monsterDamageDie.value;
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MonsterTreasureGeneratorPage');
  }

  getTitleCode(): string {

    return MonsterTreasureGeneratorPage.titleCode;
  }

  getDice(): Array<Die> {

    return Die.dice;
  }

  getMonsterOptions(): Array<MonsterOption> {

    return this.generatorService.getAllMonsterOptions();
  }

  generateTreasures() {
    
    this.monsterTreasureInputData.setMonsterOptionsFromTypeArray(this.selectedMonsterOptions);
    this.monsterTreasureInputData.setMonsterDamageDieFromValue(this.selectedMonsterDie);
    this.monsterTreasures = this.generatorService.generateTreasures(this.monsterTreasureInputData);
  }

}
