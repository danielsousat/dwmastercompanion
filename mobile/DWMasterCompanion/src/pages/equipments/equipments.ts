import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, PopoverController } from 'ionic-angular';
import { EquipmentsService } from '../../providers/equipments-service';
import { BaseFilterPage } from '../base-filter-page';
import { Equipment } from '../../dto/equipment';
import { EquipmentDetailsModal } from '../../components/equipment-details-modal/equipment-details-modal';
import { EquipmentFilter } from '../../dto/equipment-filter';
import { EquipmentPopover } from '../../components/equipment-popover/equipment-popover';

/*
  Generated class for the Equipments page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-equipments',
  templateUrl: 'equipments.html'
})
export class EquipmentsPage extends BaseFilterPage<Equipment, EquipmentFilter> {

  static titleCode: string = "EQUIPMENTS_AND_SERVICES_TITLE";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public equipmentsService: EquipmentsService, public modalCtrl: ModalController,
    private popoverCtrl: PopoverController) {
    super(equipmentsService);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EquipmentsPage');
  }

  getTitleCode(): string {

    return EquipmentsPage.titleCode;
  }

  openEquipmentDetails(equipment: Equipment) {

    let modal = this.modalCtrl.create(EquipmentDetailsModal, equipment);
    modal.present();
  }

  openTypesPopOver(ev) {

    let popover = this.popoverCtrl.create(EquipmentPopover);

    popover.present({
      ev: ev
    });

    popover.onDidDismiss(typeIndex => {

      this.typeSelected(typeIndex)
    });
  }

  typeSelected(typeIndex: number) {

    if (typeIndex != null) {

      if (typeIndex == -1) {

        this.filter.type = null; //show all  
      } else {

        this.filter.type = typeIndex;
      }

      this.filterEntities();
    }
  }

}
