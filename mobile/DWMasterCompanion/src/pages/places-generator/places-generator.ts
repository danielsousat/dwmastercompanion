import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BasePage } from '../base-page';
import { PlacesGeneratorService } from '../../providers/places-generator-service'
import { Place } from '../../dto/place'

/*
  Generated class for the PlacesGenerator page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-places-generator',
  templateUrl: 'places-generator.html'
})
export class PlacesGeneratorPage extends BasePage {

  static titleCode: string = 'PLACES_GENERATOR_TITLE';
  place: Place = null;

  constructor(public navCtrl: NavController, public generatorService : PlacesGeneratorService) {

    super();

    this.place = this.generatorService.lastGeneratedPlace;
  }

  ionViewDidLoad() {
    console.log('Hello PlacesGeneratorPage Page');
  }

  getTitleCode(): string {

    return PlacesGeneratorPage.titleCode;
  }

  generatePlace() {

    this.place = this.generatorService.generateNewPlace();

  }

}
