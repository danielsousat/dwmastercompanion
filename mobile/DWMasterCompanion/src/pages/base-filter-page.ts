import { BaseFilterProvider } from '../providers/base-filter-provider';
import { BaseFilter } from '../dto/base-filter';
import { BasePage } from './base-page';
import { Order } from '../dto/order';

/** Base class for pages that show a list of entities and provide filtering features 
 * <E> is the class type of the entity the provider will manage.
 * <F> is the class type for the filter
*/
export abstract class BaseFilterPage<E, F extends BaseFilter> extends BasePage {

    entities: Array<E>;
    filter: F;

    constructor(protected filterProvider: BaseFilterProvider<E, F>) {
        super();
        this.entities = filterProvider.getLastFilteredEntities();
        this.filter = filterProvider.getLastFilter();
    }

    filterEntities(event?: any) {

        this.entities = this.filterProvider.filterEntities(this.filter);
    }

    orderChanged(newOrder: Order) {
    
    this.filter.order = newOrder;

    this.entities = this.filterProvider.filterEntities(this.filter);
  }

}