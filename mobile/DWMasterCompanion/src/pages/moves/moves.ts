import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { BasePage } from '../base-page';
import { MovesService } from '../../providers/moves-service';
import { Move } from '../../dto/move';
import { Segment } from '../../dto/segment';
import { MoveDetailsModal } from '../../components/move-details-modal/move-details-modal';

/*
  Generated class for the Moves page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: 'page-moves',
  templateUrl: 'moves.html'
})
export class MovesPage extends BasePage {

  static titleCode: string = "MOVES_PAGE_TITLE";

  basicMoves: Array<Move>;
  specialMoves: Array<Move>;
  gmMoves: Array<Move>;
  dungeonMoves: Array<Move>;

  segments: Array<Segment> = [
    { label: "GENERAL", id: 0 },
    { label: "GM", id: 1 }
  ];

  selectedSegmentId: number = this.segments[0].id;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public movesService: MovesService, public modalCtrl: ModalController) {

    super();

    this.loadPageData();
  }

  private loadPageData() {

    this.basicMoves = this.movesService.getBasicMoves();
    this.specialMoves = this.movesService.getSpecialMoves();
    this.gmMoves = this.movesService.getGMMoves();
    this.dungeonMoves = this.movesService.getDungeonMoves();

  }

  getTitleCode(): string {

    return MovesPage.titleCode;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovesPage');
  }

  openMoveDetails(move: Move) {

    let modal = this.modalCtrl.create(MoveDetailsModal, move);
    modal.present();
  }

}
