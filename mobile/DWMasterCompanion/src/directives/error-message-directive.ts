import { Directive, ElementRef } from '@angular/core';

@Directive(
    {
        selector: '[error-message]'
    }
)
export class ErrorMessageDirective {

    constructor(el: ElementRef) {

        el.nativeElement.style.color = 'red';
    }
}