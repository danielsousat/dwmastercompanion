import { Order } from '../dto/order';

export abstract class Util {

    /**
     * returns random integer number between the interval min (inclusive) and max(exclusive)
     */
    static getRandomNumber(max: number, min: number) : number {

    return Math.floor(Math.random() * (max - min))+min;
  }

  static getArrayRandomItem(array: any[]): any {

    return array[Util.getRandomNumber(array.length, 0)];
  }

  static compareStrings(a: string, b: string, order: Order = Order.ASC): number {

    let result = a.localeCompare(b);

    return order == Order.ASC ? result : result * (-1);
  }

  static compareGeneralValues(a: any, b: any, order: Order = Order.ASC): number {

    let result: number;

    if (a > b) {
      result = 1;
    } else if (a == b) {
      result = 0;
    } else {
      result = -1;
    }

    return order == Order.ASC ? result : result * (-1);
  }
}