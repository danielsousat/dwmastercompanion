import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { TranslateUtil } from '../providers/translate-util'
import { HomePage } from '../pages/home/home';
import { PagesService } from '../providers/pages-service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  constructor(public platform: Platform, protected translateService: TranslateService, protected translateUtil: TranslateUtil,
    public pagesService: PagesService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      // this language will be used as a fallback when a translation isn't found in the current language
      this.translateService.setDefaultLang('pt-br');

      //  the lang to use, if the lang isn't available, it will use the current loader to get them
      this.translateService.use('pt-br');
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

  getPages() {

    return this.pagesService.pages;
  }
}
