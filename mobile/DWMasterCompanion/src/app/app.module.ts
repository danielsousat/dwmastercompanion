import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { LabelsPage } from '../pages/labels/labels';
import { PlacesGeneratorPage } from '../pages/places-generator/places-generator';
import { NpcGeneratorPage } from '../pages/npc-generator/npc-generator'
import { PlacesGeneratorService } from '../providers/places-generator-service';
import { NpcGeneratorService } from '../providers/npc-generator-service';
import { LabelsService } from '../providers/labels-service';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { TranslateUtil } from '../providers/translate-util';
import { ErrorMessageDirective } from'../directives/error-message-directive';
import { CommonHeader } from '../components/common-header/common-header';
import { PagesService } from '../providers/pages-service';
import { ModifiersTablePage } from '../pages/modifiers-table/modifiers-table';
import { MonsterTreasureGeneratorService } from '../providers/monster-treasure-generator-service';
import { MonsterTreasureGeneratorPage } from '../pages/monster-treasure-generator/monster-treasure-generator';
import { DamageRulesPage } from '../pages/damage-rules/damage-rules';
import { MovesService } from '../providers/moves-service';
import { MovesPage } from '../pages/moves/moves';
import { MoveDetailsModal } from '../components/move-details-modal/move-details-modal';
import { EquipmentsPage } from '../pages/equipments/equipments';
import { EquipmentsService } from '../providers/equipments-service';
import { ModalHeader } from '../components/modal-header/modal-header';
import { EquipmentDetailsModal } from '../components/equipment-details-modal/equipment-details-modal';
import { OrderButton } from '../components/order-button/order-button';
import { EquipmentPopover } from '../components/equipment-popover/equipment-popover';


export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    PlacesGeneratorPage,
    NpcGeneratorPage,
    LabelsPage,
    ModifiersTablePage,
    ErrorMessageDirective,
    CommonHeader,
    MonsterTreasureGeneratorPage,
    DamageRulesPage,
    MovesPage,
    MoveDetailsModal,
    EquipmentsPage,
    ModalHeader,
    EquipmentDetailsModal,
    OrderButton,
    EquipmentPopover
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    HttpModule,
    TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http] 
        })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    PlacesGeneratorPage,
    NpcGeneratorPage,
    ModifiersTablePage,
    LabelsPage,
    MonsterTreasureGeneratorPage,
    DamageRulesPage,
    MovesPage,
    MoveDetailsModal,
    EquipmentsPage,
    EquipmentDetailsModal,
    EquipmentPopover
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PlacesGeneratorService,
    NpcGeneratorService,
    LabelsService,
    TranslateUtil,
    PagesService,
    MonsterTreasureGeneratorService,
    MovesService,
    EquipmentsService
  ]
})
export class AppModule {}


