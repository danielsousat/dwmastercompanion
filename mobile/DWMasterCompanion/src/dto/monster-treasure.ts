export class MonsterTreasureOption {

    id: number;
    description: string;
    rollAgain: boolean = false;

    constructor(id: number, description: string, rollAgain: boolean = false) {

        this.id = id;
        this.description = description;
        this.rollAgain = rollAgain;
    }
}

export class MonsterTreasure {

    description: string;
    
    static treasureOptions: MonsterTreasureOption[] = [

        new MonsterTreasureOption(0, "MONSTER_TREASURE.TREASURE_OPTIONS.0"),
        new MonsterTreasureOption(1, "MONSTER_TREASURE.TREASURE_OPTIONS.1"),
        new MonsterTreasureOption(2, "MONSTER_TREASURE.TREASURE_OPTIONS.2"),
        new MonsterTreasureOption(3, "MONSTER_TREASURE.TREASURE_OPTIONS.3"),
        new MonsterTreasureOption(4, "MONSTER_TREASURE.TREASURE_OPTIONS.4"),
        new MonsterTreasureOption(5, "MONSTER_TREASURE.TREASURE_OPTIONS.5"),
        new MonsterTreasureOption(6, "MONSTER_TREASURE.TREASURE_OPTIONS.6"),
        new MonsterTreasureOption(7, "MONSTER_TREASURE.TREASURE_OPTIONS.7"),
        new MonsterTreasureOption(8, "MONSTER_TREASURE.TREASURE_OPTIONS.8"),
        new MonsterTreasureOption(9, "MONSTER_TREASURE.TREASURE_OPTIONS.9"),
        new MonsterTreasureOption(10, "MONSTER_TREASURE.TREASURE_OPTIONS.10"),
        new MonsterTreasureOption(11, "MONSTER_TREASURE.TREASURE_OPTIONS.11"),
        new MonsterTreasureOption(12, "MONSTER_TREASURE.TREASURE_OPTIONS.12"),
        new MonsterTreasureOption(13, "MONSTER_TREASURE.TREASURE_OPTIONS.13"),
        new MonsterTreasureOption(14, "MONSTER_TREASURE.TREASURE_OPTIONS.14", true),
        new MonsterTreasureOption(15, "MONSTER_TREASURE.TREASURE_OPTIONS.15", true),
        new MonsterTreasureOption(16, "MONSTER_TREASURE.TREASURE_OPTIONS.16", true),
        new MonsterTreasureOption(17, "MONSTER_TREASURE.TREASURE_OPTIONS.17")
    ];

    constructor(description?: string) {

        this.description = description;
    }

    static getTreasureOption(rolledNumber: number) {
        /* the array starts at zero and the rolled number starts at */
        return MonsterTreasure.treasureOptions[rolledNumber-1];
    }
}