import { Die } from '../dto/die';

export enum MonsterOptionsEnum {
    PETTY,
    TRAVELER,
    MAGICAL,
    DIVINE,
    PLANAR,
    LORD,
    ANCIENT
}

export class MonsterOption {

    type: MonsterOptionsEnum;
    nameCode: string;

    constructor(type: MonsterOptionsEnum, nameCode: string) {

        this.nameCode = nameCode;
        this.type = type;
    }
}

export class MonsterTreasureInputData {

    static allMonsterOptions: Array<MonsterOption> = [
        new MonsterOption(MonsterOptionsEnum.PETTY, "MONSTER_TREASURE.MONSTER_OPTIONS.0"),
        new MonsterOption(MonsterOptionsEnum.TRAVELER, "MONSTER_TREASURE.MONSTER_OPTIONS.1"),
        new MonsterOption(MonsterOptionsEnum.MAGICAL, "MONSTER_TREASURE.MONSTER_OPTIONS.2"),
        new MonsterOption(MonsterOptionsEnum.DIVINE, "MONSTER_TREASURE.MONSTER_OPTIONS.3"),
        new MonsterOption(MonsterOptionsEnum.PLANAR, "MONSTER_TREASURE.MONSTER_OPTIONS.4"),
        new MonsterOption(MonsterOptionsEnum.LORD, "MONSTER_TREASURE.MONSTER_OPTIONS.5"),
        new MonsterOption(MonsterOptionsEnum.ANCIENT, "MONSTER_TREASURE.MONSTER_OPTIONS.6")
    ];

    monsterDamageDie: Die = Die.dice[0]; // d4 by default
    monsterOptions: Array<MonsterOption> = new Array();

    monsterOptionsToTypeArray(): number[] {

        let ops: number[] = [];

        for (let option of this.monsterOptions) {

            ops.push(option.type);
        }

        return ops;
    }

    setMonsterOptionsFromTypeArray(types: number[]) {

        this.monsterOptions = new Array();

        for (let type of types) {

            this.monsterOptions.push(MonsterTreasureInputData.allMonsterOptions[type]);
        }
    }

    setMonsterDamageDieFromValue(value: number) {

        for (let die of Die.dice) {

            if (die.value == value) {

                this.monsterDamageDie = die;
                return;
            }
        }
    }
}