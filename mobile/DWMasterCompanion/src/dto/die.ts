import { Util } from '../util/util';

export class Die {
    name: string;
    value: number;

    static dice: Array<Die> = [
        new Die("d4", 4),
        new Die("d6", 6),
        new Die("d8", 8),
        new Die("d10", 10)
    ];

    constructor(name: string, value: number) {
        this.name = name;
        this.value = value;
    }

    roll(): number {

        return Die.rollDie(this.value);
    }

    static rollDie(value: number): number {

        return Util.getRandomNumber(value+1, 1);
    }

    static rollDice(dice: Array<Die>): number {

        let diceMaxValue: number = 0;

        for (let die of dice) {

            diceMaxValue += die.value;
        }

        console.debug("diceMaxValue=" + diceMaxValue);
        console.debug("dice.length=" + dice.length);
        /* The minimum value is the number of dice, because each die will produce a minimum value of 1.*/
        return Util.getRandomNumber(diceMaxValue+1, dice.length);
    }
}