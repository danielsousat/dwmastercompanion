import { Label } from './label';

export enum EquipmentType {

    WEAPON,
    ARMOR,
    DUNGEON_EQUIP,
    POISON,
    SERVICE,
    MEAL,
    TRANSPORTATION,
    LAND,
    BRIBERY,
    GIFT,
    TREASURE
}


/**
 * Class used to describe an equipment or service
 */
export class Equipment {

    name: string;
    description: string;
    type: EquipmentType;
    labels: Label[];
    labelsString: string = "";

    getTypeName(): string {

        return Equipment.types[this.type];
    }

    addLabel(label: Label) {

        if (this.labels == null) {

            this.labels = new Array<Label>();

            /* first added label */
            this.labelsString += label.name;
        } else {
            /* subsequent label added */
            this.labelsString += ", " + label.name;
        }

        this.labels.push(label);
    }

    /** IMPORTANT: the order of the labels must coincide with order of the EquipmentType enum. */
    static types: string[] = [
        "EQUIPMENTS.WEAPONS.LABEL",
        "EQUIPMENTS.ARMORS.LABEL",
        "EQUIPMENTS.DUNGEON_EQUIPS.LABEL",
        "EQUIPMENTS.POISONS.LABEL",
        "EQUIPMENTS.SERVICES.LABEL",
        "EQUIPMENTS.MEALS.LABEL",
        "EQUIPMENTS.TRANSPORTATION.LABEL",
        "EQUIPMENTS.LANDS.LABEL",
        "EQUIPMENTS.BRIBERY.LABEL",
        "EQUIPMENTS.GIFTS.LABEL",
        "EQUIPMENTS.TREASURES.LABEL"
    ];
}