export enum MoveType {
    BASIC,
    SPECIAL,
    GM,
    DUNGEON
}

export class Move {
    
    static types: string[] = [
        "MOVES.BASIC.LABEL",
        "MOVES.SPECIAL.LABEL",
        "MOVES.GM.LABEL",
        "MOVES.DUNGEON.LABEL"
    ];
    
    header: string;
    description: string;

    constructor(header: string, description?: string) {

        this.header = header;
        this.description = description;
    }
}