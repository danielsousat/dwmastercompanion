import { Order } from './order';

export class BaseFilter {

    order: Order = Order.ASC;
    sortField: string;
}