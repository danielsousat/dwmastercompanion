import { EquipmentType } from './equipment';
import { BaseFilter } from './base-filter';

export class EquipmentFilter extends BaseFilter {

    name: string = "";
    type: EquipmentType;
}