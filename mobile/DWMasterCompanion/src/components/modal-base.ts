import { NavParams, ViewController } from 'ionic-angular';

export class ModalBase<T> {

    entity: T;
    
    constructor(public params: NavParams,
      public viewCtrl: ViewController) {
    
      this.entity = params.data;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}