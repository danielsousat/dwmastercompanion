import { Component, Input } from '@angular/core';
/**
 * Common header used in the app pages. It contains the side menu button and the page title.
 */
@Component(
    {
        selector: 'common-header',
        templateUrl: 'common-header.html'
    }
)
export class CommonHeader {

    @Input() 
    pageTitleCode: string;


    
}