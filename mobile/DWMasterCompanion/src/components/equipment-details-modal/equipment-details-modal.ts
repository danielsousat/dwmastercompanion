import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Equipment } from '../../dto/equipment';
import { ModalBase } from '../modal-base';

@Component({
  templateUrl: 'equipment-details-modal.html'
})
export class EquipmentDetailsModal extends ModalBase<Equipment> {

  constructor(public params: NavParams,
    public viewCtrl: ViewController) {
    
    super(params, viewCtrl);

  }
}