import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Equipment } from '../../dto/equipment';

@Component({
  templateUrl: 'equipment-popover.html'
})
export class EquipmentPopover {

  constructor(public params: NavParams,
    public viewCtrl: ViewController) {
    
  }

  getTypes(): string[] {

    return Equipment.types;
  }

  typeClicked(typeIndex: number) {

    this.viewCtrl.dismiss(typeIndex);
  }
}