import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Order } from '../../dto/order';

class ButtonState {

    label: String;
    icon: String;
    order: Order;

    constructor(label: String, icon: String, order: Order) {

        this.label = label;
        this.icon = icon;
        this.order = order;
    }
}

/**
 * Common header used in the app pages. It contains the side menu button and the page title.
 */
@Component(
    {
        selector: 'order-button',
        templateUrl: 'order-button.html'
    }
)
export class OrderButton {
    
    /** When button is toggled, the toggled event is emmitted with the new Order state */
    @Output() orderChange = new EventEmitter<Order>();

    static ASC_STATE: ButtonState = new ButtonState("Az", "arrow-round-down", Order.ASC);
    static DESC_STATE: ButtonState = new ButtonState("Za", "arrow-round-up", Order.DESC);

    currentState: ButtonState = OrderButton.ASC_STATE;

    @Input()
    get order(): Order {

        return this.currentState.order;
    }

    set order(order: Order) {

        this.changeButtonState(order);

        this.orderChange.emit(order);
    }

    changeButtonState(order: Order) {

        if (order == Order.ASC){
            this.currentState = OrderButton.ASC_STATE;
        } else {
            this.currentState = OrderButton.DESC_STATE;
        }
    }

    toggle() {

        let order: Order = this.currentState.order == Order.ASC? Order.DESC: Order.ASC;

        this.order = order;
    }
}