import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Move } from '../../dto/move';
import { ModalBase } from '../modal-base';

@Component({
  templateUrl: 'move-details-modal.html'
})
export class MoveDetailsModal extends ModalBase<Move> {

  constructor(public params: NavParams,
    public viewCtrl: ViewController) {

    super(params, viewCtrl);
  }
}