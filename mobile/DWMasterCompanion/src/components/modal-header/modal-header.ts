import { Component, Output, EventEmitter } from '@angular/core';
/**
 * Common header used in the app pages. It contains the side menu button and the page title.
 */
@Component(
    {
        selector: 'modal-header',
        templateUrl: 'modal-header.html'
    }
)
export class ModalHeader {
    
    @Output() dismissed = new EventEmitter<any>();

    dismiss() {

        this.dismissed.emit();
    }
}