/**
 * Error object that has a message code to be translated and the params for the message.
 */
export class TranslatableError  {

    messageCode: string = null;
    messageParams: Object = null;

    constructor(messageCode: string, messageParams?: Object) {

        this.messageCode = messageCode;
        this.messageParams = messageParams;
    }

}