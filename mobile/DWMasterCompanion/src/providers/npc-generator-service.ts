import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Npc } from '../dto/npc';
import { TranslateUtil } from './translate-util';
import { Util } from '../util/util';
import { CustomValidators } from '../validators/custom-validators';
import { TranslatableError } from '../exception/translatable-error'

/*
  Generated class for the NpcGeneratorService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NpcGeneratorService {

  static NUM_MAX_PERSONALITIES: number = 10;
  static NUM_MIN_PERSONALITIES: number = 1;
  static DEFAULT_NUM_PERSONALITIES: number = 3;

  lastNumberOfPersonalities: number = NpcGeneratorService.DEFAULT_NUM_PERSONALITIES;
  lastGenerated: Npc = null;

  errors = {
    numberOfPersonalities: {
      code: "ERROR.NUM_CHAR_OUT_OF_BOUND", 
      params: {min: NpcGeneratorService.NUM_MIN_PERSONALITIES, max: NpcGeneratorService.NUM_MAX_PERSONALITIES}
    }
  };

  constructor(public http: Http, public translateUtil: TranslateUtil) {
    console.log('Hello NpcGeneratorService Provider');
  }

  /**
   * Throws a TranslatableError, if the numberOfPersonalities is out of the min and max range.
   */
  validateNumberOfPersonalities(numberOfPersonalities: number) {

    if (
      !CustomValidators.maxValueCheck(numberOfPersonalities, NpcGeneratorService.NUM_MAX_PERSONALITIES)
      || !CustomValidators.minValueCheck(numberOfPersonalities, NpcGeneratorService.NUM_MIN_PERSONALITIES)
      ) {

        throw new TranslatableError(this.errors.numberOfPersonalities.code, 
          this.errors.numberOfPersonalities.params); 
      }
    
  }

  generateNpc(numberOfPersonalities: number): Npc {

    this.validateNumberOfPersonalities(numberOfPersonalities);

    this.lastGenerated = new Npc();
    this.lastNumberOfPersonalities = numberOfPersonalities;

    this.lastGenerated.name = Util.getArrayRandomItem(Npc.names);
    this.lastGenerated.personality = this.generatePersonalities(numberOfPersonalities);

    return this.lastGenerated;
  }

  generatePersonalities(numberOfPersonalities: number): string[] {

    let list: string[] = [];

    for (let i = 0; i < numberOfPersonalities && i < NpcGeneratorService.NUM_MAX_PERSONALITIES; i++) {

      list.push(this.translateUtil.getRandomArrayItemTranslated(Npc.personalities));
    }

    return list;
  }

  getErrorMessage(field: string): string {

    return this.translateUtil.translate(
      this.errors[field].code,
      this.errors[field].params);
  }

}
