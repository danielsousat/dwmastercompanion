import { Util } from '../util/util';
import { Order } from '../dto/order';
import { BaseFilter } from '../dto/base-filter';

/** Base class for providers that control a list of entities and provide filtering features 
 * <E> is the class type of the entity the provider will manage.
 * <F> is the class type for the filter
*/
export abstract class BaseFilterProvider<E, F extends BaseFilter> {

    protected lastFilteredEntities: Array<E>;
    protected lastFilter: F;


    getLastFilteredEntities(): Array<E> {

        return this.lastFilteredEntities;
    }

    getLastFilter(): F {

        return this.lastFilter;
    }

    reset() {

        this.lastFilter = this.newFilter();
        this.lastFilter.sortField = this.getDefaultSortField();
        this.lastFilteredEntities = this.filterEntities(this.lastFilter);
    }

    filterEntities(filter: F): Array<E> {

        this.lastFilteredEntities = this.applyFilter(filter);
        this.lastFilteredEntities = this.sortEntities(this.lastFilteredEntities, filter.sortField, filter.order);

        return this.lastFilteredEntities;

    }

    /**
     * Sorts the entities based on a field from it and the order. The sort makes a comparison depending on the filed type.
     * If you need another kind of sort, just override this method.
     * 
     * @param orderField which field of the entity should the entities be sorted
     * @param order can be "asc" or "desc"
     * 
     * @return returns the filtered entities array
     */
    sortEntities(entitiesToBeFiltered: Array<E>, orderField: string = this.getDefaultSortField(), order: Order = Order.ASC): Array<E> {

        return entitiesToBeFiltered.sort((a, b) => {

            if (typeof a[orderField] === "string") {

                return Util.compareStrings(a[orderField], b[orderField], order);
            } else {
                return Util.compareGeneralValues(a[orderField], b[orderField], order);
            }
        })
    }

    protected filterEntitiesByString(entities: Array<E>, entityFieldName: string, fieldFilterValue: string): Array<E> {

        if (fieldFilterValue == null) {

            return entities;
        } else {

            return entities.filter((entity) => {

                return entity[entityFieldName].toLowerCase().includes(fieldFilterValue.toLowerCase());
            });
        }
    }

    /**
     * General filter function, that will make the comparison using equal (==) on the field.
     */
    protected filterEntitiesByEquality(entities: Array<E>, entityFieldName: string, fieldFilterValue): Array<E> {
        
        if (fieldFilterValue == null) {

            return entities;
        }

        return entities.filter((entity) => {

            return entity[entityFieldName] == fieldFilterValue;
        });
    }

    protected abstract newFilter(): F;

    protected abstract applyFilter(filter: F): Array<E>;

    protected abstract getDefaultSortField(): string;
}