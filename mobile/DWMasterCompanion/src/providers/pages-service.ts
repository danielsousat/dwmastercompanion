import { Injectable } from '@angular/core';
import { PlacesGeneratorPage } from '../pages/places-generator/places-generator';
import { NpcGeneratorPage } from '../pages/npc-generator/npc-generator';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { LabelsPage } from '../pages/labels/labels';
import { ModifiersTablePage } from '../pages/modifiers-table/modifiers-table';
import { MonsterTreasureGeneratorPage } from '../pages/monster-treasure-generator/monster-treasure-generator';
import { DamageRulesPage } from '../pages/damage-rules/damage-rules';
import { MovesPage } from '../pages/moves/moves';
import { EquipmentsPage } from '../pages/equipments/equipments';

@Injectable()
export class PagesService {

  pages: Array<{ title: string, component: any }>;

  constructor() {

    this.initPages();
  }

  initPages() {

    this.pages = [
      { title: HomePage.titleCode, component: HomePage },
      { title: PlacesGeneratorPage.titleCode, component: PlacesGeneratorPage },
      { title: NpcGeneratorPage.titleCode, component: NpcGeneratorPage },
      { title: MonsterTreasureGeneratorPage.titleCode, component: MonsterTreasureGeneratorPage },
      { title: LabelsPage.titleCode, component: LabelsPage },
      { title: EquipmentsPage.titleCode, component: EquipmentsPage },
      { title: MovesPage.titleCode, component: MovesPage },
      { title: ModifiersTablePage.titleCode, component: ModifiersTablePage },
      { title: DamageRulesPage.titleCode, component: DamageRulesPage },
      { title: AboutPage.titleCode, component: AboutPage }
    ];
  }

  getPagesWithoutHomeAndAbout() {

    return this.pages.slice(1, this.pages.length-1);
  }
}