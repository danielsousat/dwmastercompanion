import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MonsterTreasureInputData, MonsterOption, MonsterOptionsEnum } from '../dto/monster-treasure-input-data';
import { Die } from '../dto/die';
import { MonsterTreasure, MonsterTreasureOption } from '../dto/monster-treasure';
import { TranslateUtil } from './translate-util';
import { Util } from '../util/util';

/*
  Generated class for the MonsterTreasureGeneratorService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MonsterTreasureGeneratorService {

  lastMonsterTreasureInputData: MonsterTreasureInputData = new MonsterTreasureInputData;
  lastMonsterTreasuresGenerated: Array<MonsterTreasure>;

  constructor(public http: Http, public translateUtil: TranslateUtil) {
    console.log('Hello MonsterTreasureGeneratorService Provider');
  }

  getAllMonsterOptions(): Array<MonsterOption> {

    return MonsterTreasureInputData.allMonsterOptions;
  }

  generateTreasures(monsterTreasureInputData: MonsterTreasureInputData): Array<MonsterTreasure> {

    let rollAgain = false;
    let rollTwice = false;
    let dice: Array<Die> = [monsterTreasureInputData.monsterDamageDie];

    this.lastMonsterTreasureInputData = monsterTreasureInputData;
    this.lastMonsterTreasuresGenerated = new Array<MonsterTreasure>();

    rollTwice = this.checkMonsterOptions(monsterTreasureInputData.monsterOptions, dice, this.lastMonsterTreasuresGenerated);

    do {
      
      let rollResult: number;
      let treasure: MonsterTreasure = new MonsterTreasure();
      let treasureOption: MonsterTreasureOption;

      rollResult = this.rollTreasure(dice, rollTwice);

      console.debug("rollResult: " + rollResult);

      treasureOption = MonsterTreasure.getTreasureOption(rollResult);

      rollAgain = treasureOption.rollAgain;

      treasure.description = this.getMainTreasure(treasureOption);

      this.lastMonsterTreasuresGenerated.unshift(treasure); //adding to beginning of the array

    } while (rollAgain)

    return this.lastMonsterTreasuresGenerated;
  }

  /**
   * May add dice to the dice param, may add treasures to the monsterTreasures param.
   * 
   * returns the value of rollTwice.
   */
  private checkMonsterOptions(monsterOptions: Array<MonsterOption>, dice: Array<Die>,
    monsterTreasures: Array<MonsterTreasure>): boolean {

    let rollTwice = false;

    for (let monsterOption of monsterOptions) {

      switch (monsterOption.type) {

        case MonsterOptionsEnum.PETTY:
          rollTwice = true;
          break;
        case MonsterOptionsEnum.TRAVELER:
          monsterTreasures.push(new MonsterTreasure(this.translateUtil.translate("MONSTER_TREASURE.ADDED_TREASURES.0")));
          break;
        case MonsterOptionsEnum.MAGICAL:
          monsterTreasures.push(new MonsterTreasure(this.translateUtil.translate("MONSTER_TREASURE.ADDED_TREASURES.1")));
          break;
        case MonsterOptionsEnum.DIVINE:
          monsterTreasures.push(new MonsterTreasure(this.translateUtil.translate("MONSTER_TREASURE.ADDED_TREASURES.2")));
          break;
        case MonsterOptionsEnum.PLANAR:
          monsterTreasures.push(new MonsterTreasure(this.translateUtil.translate("MONSTER_TREASURE.ADDED_TREASURES.3")));
          break;
        case MonsterOptionsEnum.LORD:
        case MonsterOptionsEnum.ANCIENT:
          /* push a d4 */
          dice.push(Die.dice[0]);
          break;
        default:
          break;
      }
    }

    return rollTwice;
  }

  private getMainTreasure(treasureOption: MonsterTreasureOption): string {

    let id: number = treasureOption.id;
    let params;

    switch (id) {

      case 0:
        params = { amount: this.getAmount(2, 16) };
        break;
      case 2:
        params = { amount: this.getAmount(4, 40) };
        break;
      case 3:
        params = { amount: this.getAmount(2, 20, 10) };
        break;
      case 6:
        params = { amount: this.getAmount(1, 4, 100) };
        break;
      case 7:
        params = { amount: this.getAmount(2, 12, 100) };
        break;
      case 8:
        params = { amount: this.getAmount(3, 18, 100) };
        break;
      case 10:
        params = { amount: this.getAmount(2, 8, 100) };
        break;
      case 11:
        params = { amount: this.getAmount(3, 12, 100) };
        break;
      case 12:
        params = { amount: this.getAmount(4, 16, 100) }
        break;
      case 13:
        params = { amount: this.getAmount(5, 20, 100) }
        break;
      case 17:
        params = {
          coins_amount: this.getAmount(1, 10, 1000),
          gems_amount: this.getAmount(1, 10, 10),
          gems_value: this.getAmount(2, 12, 100)
        }
        break;
      default:
        break;
    }

    return this.translateUtil.translate(treasureOption.description, params);
  }

  private getAmount(min: number, max: number, multiplier: number = 1): string {

    return (Util.getRandomNumber(max + 1, min) * multiplier).toString();
  }

  private rollTreasure(dice: Array<Die>, rollTwice: boolean): number {

    let roll: number = Die.rollDice(dice);

    if (rollTwice) {
      /* roll again and keep the greater result. */
      let roll2: number = Die.rollDice(dice);

      return roll > roll2 ? roll : roll2;
    }

    return roll;
  }

}
