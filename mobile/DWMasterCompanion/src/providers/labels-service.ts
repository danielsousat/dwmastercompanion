import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Label } from '../dto/label'
import { TranslateUtil } from './translate-util';
import { LabelFilter } from '../dto/label-filter';
import { BaseFilterProvider } from './base-filter-provider';

class LabelAndParam {

  label: Label;
  nameParam: Object;

  constructor(label: Label, nameParam?: Object) {

    this.label = label;
    this.nameParam = nameParam;
  }
}

/*
  Generated class for the LabelsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LabelsService extends BaseFilterProvider<Label, LabelFilter>{

  private translatedLabels: Array<Label>;


  constructor(public http: Http, public translateUtil: TranslateUtil) {

    super();

    this.translateLabels();

    this.reset();

    console.log('Hello LabelsService Provider');
  }

  protected newFilter(): LabelFilter {

    return new LabelFilter();
  }

  protected applyFilter(filter: LabelFilter): Array<Label> {

    return this.filterEntitiesByString(this.translatedLabels, "name", filter.name);
  }

  protected getDefaultSortField(): string {

    return "name";
  }

  getLabelById(id: number): Label {

    return this.translatedLabels[id];
  }

  getLabelTranslatedWithParams(labelId: number, params: Object): Label {

    let unstranslatedLabelName: string = LabelsService.labels[labelId].label.name;
    let translatedDescription: string = this.translatedLabels[labelId].description;

    return new Label(this.translateUtil.translate(unstranslatedLabelName, params), translatedDescription);

  }

  private translateLabels() {

    this.translatedLabels = new Array<Label>();

    for (let labelAndParam of LabelsService.labels) {
      let newLabel = new Label(this.translateUtil.translate(labelAndParam.label.name, labelAndParam.nameParam),

        this.translateUtil.translate(labelAndParam.label.description));

      this.translatedLabels.push(newLabel);

    }
  }

  //This attribute is defined here for better view of the class code that matters.
  private static labels: LabelAndParam[] = [
    new LabelAndParam(new Label("LABELS.NAME.0", "LABELS.DESCRIPTION.0")),
    new LabelAndParam(new Label("LABELS.NAME.1", "LABELS.DESCRIPTION.1")),
    new LabelAndParam(new Label("LABELS.NAME.2", "LABELS.DESCRIPTION.2")),
    new LabelAndParam(new Label("LABELS.NAME.3", "LABELS.DESCRIPTION.3")),
    new LabelAndParam(new Label("LABELS.NAME.4", "LABELS.DESCRIPTION.4")),
    new LabelAndParam(new Label("LABELS.NAME.5", "LABELS.DESCRIPTION.5"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.6", "LABELS.DESCRIPTION.6"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.7", "LABELS.DESCRIPTION.7")),
    new LabelAndParam(new Label("LABELS.NAME.8", "LABELS.DESCRIPTION.8")),
    new LabelAndParam(new Label("LABELS.NAME.9", "LABELS.DESCRIPTION.9")),
    new LabelAndParam(new Label("LABELS.NAME.10", "LABELS.DESCRIPTION.10")),
    new LabelAndParam(new Label("LABELS.NAME.11", "LABELS.DESCRIPTION.11")),
    new LabelAndParam(new Label("LABELS.NAME.12", "LABELS.DESCRIPTION.12")),
    new LabelAndParam(new Label("LABELS.NAME.13", "LABELS.DESCRIPTION.13")),
    new LabelAndParam(new Label("LABELS.NAME.14", "LABELS.DESCRIPTION.14")),
    new LabelAndParam(new Label("LABELS.NAME.15", "LABELS.DESCRIPTION.15")),
    new LabelAndParam(new Label("LABELS.NAME.16", "LABELS.DESCRIPTION.16"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.17", "LABELS.DESCRIPTION.17")),
    new LabelAndParam(new Label("LABELS.NAME.18", "LABELS.DESCRIPTION.18")),
    new LabelAndParam(new Label("LABELS.NAME.19", "LABELS.DESCRIPTION.19")),
    new LabelAndParam(new Label("LABELS.NAME.20", "LABELS.DESCRIPTION.20")),
    new LabelAndParam(new Label("LABELS.NAME.21", "LABELS.DESCRIPTION.21")),
    new LabelAndParam(new Label("LABELS.NAME.22", "LABELS.DESCRIPTION.22")),
    new LabelAndParam(new Label("LABELS.NAME.23", "LABELS.DESCRIPTION.23")),
    new LabelAndParam(new Label("LABELS.NAME.24", "LABELS.DESCRIPTION.24")),
    new LabelAndParam(new Label("LABELS.NAME.25", "LABELS.DESCRIPTION.25")),
    new LabelAndParam(new Label("LABELS.NAME.26", "LABELS.DESCRIPTION.26")),
    new LabelAndParam(new Label("LABELS.NAME.27", "LABELS.DESCRIPTION.27")),
    new LabelAndParam(new Label("LABELS.NAME.28", "LABELS.DESCRIPTION.28")),
    new LabelAndParam(new Label("LABELS.NAME.29", "LABELS.DESCRIPTION.29")),
    new LabelAndParam(new Label("LABELS.NAME.30", "LABELS.DESCRIPTION.30")),
    new LabelAndParam(new Label("LABELS.NAME.31", "LABELS.DESCRIPTION.31")),
    new LabelAndParam(new Label("LABELS.NAME.32", "LABELS.DESCRIPTION.32")),
    new LabelAndParam(new Label("LABELS.NAME.33", "LABELS.DESCRIPTION.33")),
    new LabelAndParam(new Label("LABELS.NAME.34", "LABELS.DESCRIPTION.34")),
    new LabelAndParam(new Label("LABELS.NAME.35", "LABELS.DESCRIPTION.35")),
    new LabelAndParam(new Label("LABELS.NAME.36", "LABELS.DESCRIPTION.36")),
    new LabelAndParam(new Label("LABELS.NAME.37", "LABELS.DESCRIPTION.37")),
    new LabelAndParam(new Label("LABELS.NAME.38", "LABELS.DESCRIPTION.38")),
    new LabelAndParam(new Label("LABELS.NAME.39", "LABELS.DESCRIPTION.39")),
    new LabelAndParam(new Label("LABELS.NAME.40", "LABELS.DESCRIPTION.40")),
    new LabelAndParam(new Label("LABELS.NAME.41", "LABELS.DESCRIPTION.41")),
    new LabelAndParam(new Label("LABELS.NAME.42", "LABELS.DESCRIPTION.42")),
    new LabelAndParam(new Label("LABELS.NAME.43", "LABELS.DESCRIPTION.43")),
    new LabelAndParam(new Label("LABELS.NAME.44", "LABELS.DESCRIPTION.44")),
    new LabelAndParam(new Label("LABELS.NAME.45", "LABELS.DESCRIPTION.45")),
    new LabelAndParam(new Label("LABELS.NAME.46", "LABELS.DESCRIPTION.46")),
    new LabelAndParam(new Label("LABELS.NAME.47", "LABELS.DESCRIPTION.47")),
    new LabelAndParam(new Label("LABELS.NAME.48", "LABELS.DESCRIPTION.48")),
    new LabelAndParam(new Label("LABELS.NAME.49", "LABELS.DESCRIPTION.49")),
    new LabelAndParam(new Label("LABELS.NAME.50", "LABELS.DESCRIPTION.50")),
    new LabelAndParam(new Label("LABELS.NAME.51", "LABELS.DESCRIPTION.51"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.52", "LABELS.DESCRIPTION.52"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.53", "LABELS.DESCRIPTION.53")),
    new LabelAndParam(new Label("LABELS.NAME.54", "LABELS.DESCRIPTION.54")),
    new LabelAndParam(new Label("LABELS.NAME.55", "LABELS.DESCRIPTION.55")),
    new LabelAndParam(new Label("LABELS.NAME.56", "LABELS.DESCRIPTION.56"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.57", "LABELS.DESCRIPTION.57")),
    new LabelAndParam(new Label("LABELS.NAME.58", "LABELS.DESCRIPTION.58")),
    new LabelAndParam(new Label("LABELS.NAME.59", "LABELS.DESCRIPTION.59")),
    new LabelAndParam(new Label("LABELS.NAME.60", "LABELS.DESCRIPTION.60")),
    new LabelAndParam(new Label("LABELS.NAME.61", "LABELS.DESCRIPTION.61"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.62", "LABELS.DESCRIPTION.62")),
    new LabelAndParam(new Label("LABELS.NAME.63", "LABELS.DESCRIPTION.63")),
    new LabelAndParam(new Label("LABELS.NAME.64", "LABELS.DESCRIPTION.64")),
    new LabelAndParam(new Label("LABELS.NAME.65", "LABELS.DESCRIPTION.65")),
    new LabelAndParam(new Label("LABELS.NAME.66", "LABELS.DESCRIPTION.66")),
    new LabelAndParam(new Label("LABELS.NAME.67", "LABELS.DESCRIPTION.67")),
    new LabelAndParam(new Label("LABELS.NAME.68", "LABELS.DESCRIPTION.68")),
    new LabelAndParam(new Label("LABELS.NAME.69", "LABELS.DESCRIPTION.69")),
    new LabelAndParam(new Label("LABELS.NAME.70", "LABELS.DESCRIPTION.70")),
    new LabelAndParam(new Label("LABELS.NAME.71", "LABELS.DESCRIPTION.71")),
    new LabelAndParam(new Label("LABELS.NAME.72", "LABELS.DESCRIPTION.72")),
    new LabelAndParam(new Label("LABELS.NAME.73", "LABELS.DESCRIPTION.73")),
    new LabelAndParam(new Label("LABELS.NAME.74", "LABELS.DESCRIPTION.74")),
    new LabelAndParam(new Label("LABELS.NAME.75", "LABELS.DESCRIPTION.75")),
    new LabelAndParam(new Label("LABELS.NAME.76", "LABELS.DESCRIPTION.76")),
    new LabelAndParam(new Label("LABELS.NAME.77", "LABELS.DESCRIPTION.77")),
    new LabelAndParam(new Label("LABELS.NAME.78", "LABELS.DESCRIPTION.78")),
    new LabelAndParam(new Label("LABELS.NAME.79", "LABELS.DESCRIPTION.79")),
    new LabelAndParam(new Label("LABELS.NAME.80", "LABELS.DESCRIPTION.80"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.81", "LABELS.DESCRIPTION.81")),
    new LabelAndParam(new Label("LABELS.NAME.82", "LABELS.DESCRIPTION.82"), { n: "n" }),
    new LabelAndParam(new Label("LABELS.NAME.83", "LABELS.DESCRIPTION.83"), { n: "n" })

  ];

}
