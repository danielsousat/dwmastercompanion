import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Equipment, EquipmentType } from '../dto/equipment';
import { TranslateUtil } from './translate-util';
import { LabelsService } from './labels-service';
import { EquipmentFilter } from '../dto/equipment-filter';
import { BaseFilterProvider } from './base-filter-provider';

class CustomParam {

  text: string;
  toBeTranslated: boolean

  constructor(text: string, toBeTranslated: boolean = false) {

    this.text = text;
    this.toBeTranslated = toBeTranslated;
  }
}

class LabelParam {

  id: number;
  params: Object;
  additionalText: string;
  customParams: CustomParam[];

  constructor(id: number, params?: Object, additionalText?: string, customParams?: CustomParam[]) {

    this.id = id;
    this.params = params;
    this.additionalText = additionalText;
    this.customParams = customParams;
  }
}
/*
  Generated class for the EquipmentService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class EquipmentsService extends BaseFilterProvider<Equipment, EquipmentFilter> {

  private equipments: Equipment[];

  constructor(public http: Http, public translateUtil: TranslateUtil,
    public labelsService: LabelsService) {
    
    super();
    console.log('Hello EquipmentService Provider');

    this.initEquipments();
    this.reset();
  }

  getEquipments(): Equipment[] {

    return this.equipments;
  }

  protected newFilter(): EquipmentFilter {

    return new EquipmentFilter();
  }

  protected getDefaultSortField(): string {

    return "name";
  }

  protected applyFilter(filter: EquipmentFilter): Array<Equipment> {

    let filtered: Array<Equipment>;

    filtered = this.filterEntitiesByString(this.equipments, "name", filter.name);
    filtered = this.filterEntitiesByEquality(filtered, "type", filter.type);

    return filtered;

  }
 
  /*---------------- init provider methods -----------------*/

  private addEquipment(name: string, type: EquipmentType, labels: LabelParam[], description?: string) {

    let newEq = new Equipment();

    newEq.name = this.translateUtil.translate(name);
    newEq.type = type;

    this.fillLabels(labels, newEq);

    if (description != null) {
      newEq.description = this.translateUtil.translate(description);
    }

    this.equipments.push(newEq);

  }

  private fillLabels(labels: LabelParam[], equipment: Equipment) {

    for (let l of labels) {
      
      if (l.customParams != null) {
        l.params = this.buildCustomParam(l.customParams);
      }

      let newLabel = l.params == null ? this.labelsService.getLabelById(l.id) : this.labelsService.getLabelTranslatedWithParams(l.id, l.params);

      if (l.additionalText != null) {
        newLabel.name += " " + this.translateUtil.translate(l.additionalText);
      }

      equipment.addLabel(newLabel);
    }
  }

  private buildCustomParam(customParams: CustomParam[]): Object {

    let param: string = "";

    for (let c of customParams) {

      param += c.toBeTranslated ? this.translateUtil.translate(c.text) : c.text;
    }

    return {n: param};
  }

  private initEquipments() {

    this.equipments = new Array<Equipment>();
    
    /* Weapons */
    this.addEquipment("EQUIPMENTS.WEAPONS.0.NAME", EquipmentType.WEAPON, 
      [new LabelParam(66), new LabelParam(51, {n: 15}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.1.NAME", EquipmentType.WEAPON, 
      [new LabelParam(66), new LabelParam(19), new LabelParam(51, {n: 60}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.2.NAME", EquipmentType.WEAPON, 
      [new LabelParam(66), new LabelParam(19), new LabelParam(51, {n: 100}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.3.NAME", EquipmentType.WEAPON, 
      [new LabelParam(66), new LabelParam(16, {n: 1}), new LabelParam(69), new LabelParam(51, {n: 35}), new LabelParam(61, {n: 3})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.4.NAME", EquipmentType.WEAPON, 
      [new LabelParam(52, {n: 3}), new LabelParam(51, {n: 1}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.5.NAME", EquipmentType.WEAPON, 
      [new LabelParam(52, {n: 4}), new LabelParam(51, {n: 20}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.6.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(51, {n: 1}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.7.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(51, {n: 1}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.8.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(21), new LabelParam(51, {n: 1}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.9.NAME", EquipmentType.WEAPON, 
      [new LabelParam(46), new LabelParam(51, {n: 2}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.10.NAME", EquipmentType.WEAPON, 
      [new LabelParam(46), new LabelParam(51, {n: 2}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.11.NAME", EquipmentType.WEAPON, 
      [new LabelParam(46), new LabelParam(51, {n: 2}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.12.NAME", EquipmentType.WEAPON, 
      [new LabelParam(7), new LabelParam(66), new LabelParam(51, {n: 1}), new LabelParam(61, {n: 0})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.13.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(51, {n: 8}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.14.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(51, {n: 8}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.15.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(51, {n: 8}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.16.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(51, {n: 8}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.17.NAME", EquipmentType.WEAPON, 
      [new LabelParam(0), new LabelParam(7), new LabelParam(66), new LabelParam(51, {n: 5}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.18.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(16, {n: 1}), new LabelParam(51, {n: 15}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.19.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(16, {n: 1}), new LabelParam(51, {n: 15}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.20.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(16, {n: 1}), new LabelParam(51, {n: 15}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.21.NAME", EquipmentType.WEAPON, 
      [new LabelParam(0), new LabelParam(16, {n: 1}), new LabelParam(21), new LabelParam(51, {n: 9}), new LabelParam(61, {n: 2})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.22.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(65), new LabelParam(51, {n: 25}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.WEAPONS.23.NAME", EquipmentType.WEAPON, 
      [new LabelParam(13), new LabelParam(56, {n: 1}), new LabelParam(65), new LabelParam(51, {n: 50}), new LabelParam(61, {n: 2})]);
    
    /* armors */
    this.addEquipment("EQUIPMENTS.ARMORS.0.NAME", EquipmentType.ARMOR, 
      [new LabelParam(5, {n: 1}), new LabelParam(81), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.ARMORS.1.NAME", EquipmentType.ARMOR, 
      [new LabelParam(5, {n: 1}), new LabelParam(81), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 1})]);
    this.addEquipment("EQUIPMENTS.ARMORS.2.NAME", EquipmentType.ARMOR, 
      [new LabelParam(5, {n: 2}), new LabelParam(81), new LabelParam(18), new LabelParam(51, {n: 50}), new LabelParam(61, {n: 3})]);
    this.addEquipment("EQUIPMENTS.ARMORS.3.NAME", EquipmentType.ARMOR, 
      [new LabelParam(5, {n: 3}), new LabelParam(81), new LabelParam(18), new LabelParam(51, {n: 350}), new LabelParam(61, {n: 4})]);
    this.addEquipment("EQUIPMENTS.ARMORS.4.NAME", EquipmentType.ARMOR, 
      [new LabelParam(6, {n: 1}), new LabelParam(51, {n: 15}), new LabelParam(61, {n: 2})]);

    /* Dungeon Equips */
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.0.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(80, {n: 5}), new LabelParam(51, {n: 20}), new LabelParam(61, {n: 1})], "EQUIPMENTS.DUNGEON_EQUIPS.0.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.1.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(80, {n: 3}), new LabelParam(43), new LabelParam(51, {n: 5}), new LabelParam(61, {n: 0})], "EQUIPMENTS.DUNGEON_EQUIPS.1.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.2.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(80, {n: 2}), new LabelParam(43), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 1})], "EQUIPMENTS.DUNGEON_EQUIPS.2.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.3.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(51, {n: 50}), new LabelParam(61, {n: 0})], "EQUIPMENTS.DUNGEON_EQUIPS.3.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.4.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(51, {n: 10}), new LabelParam(61, {n: 4})], "EQUIPMENTS.DUNGEON_EQUIPS.4.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.5.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(80, {n: 5}), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 2})], "EQUIPMENTS.DUNGEON_EQUIPS.5.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.6.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(51, {n: 10}), new LabelParam(61, {n: 0})], "EQUIPMENTS.DUNGEON_EQUIPS.6.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.7.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(67), new LabelParam(80, {n: 5}), new LabelParam(51, {n: 3}), new LabelParam(61, {n: 1})], "EQUIPMENTS.DUNGEON_EQUIPS.7.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.8.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(67), new LabelParam(80, {n: 1}), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 1})], "EQUIPMENTS.DUNGEON_EQUIPS.8.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.9.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(68, null, "LABELS.NAME.2"), new LabelParam(67), new LabelParam(80, {n: 7}), new LabelParam(51, {n: 3}), new LabelParam(61, {n: 1})], "EQUIPMENTS.DUNGEON_EQUIPS.9.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.10.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(67), new LabelParam(80, {n: 7}), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 1})], "EQUIPMENTS.DUNGEON_EQUIPS.10.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.DUNGEON_EQUIPS.11.NAME", EquipmentType.DUNGEON_EQUIP, 
      [new LabelParam(80, {n: 6}), new LabelParam(51, {n: 5}), new LabelParam(61, {n: 0})], "EQUIPMENTS.DUNGEON_EQUIPS.11.DESCRIPTION");

    /* Poisons */
    this.addEquipment("EQUIPMENTS.POISONS.0.NAME", EquipmentType.POISON, 
      [new LabelParam(58), new LabelParam(3), new LabelParam(51, {n: 15}), new LabelParam(61, {n: 0})], "EQUIPMENTS.POISONS.0.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.POISONS.1.NAME", EquipmentType.POISON, 
      [new LabelParam(58), new LabelParam(79), new LabelParam(51, {n: 12}), new LabelParam(61, {n: 0})], "EQUIPMENTS.POISONS.1.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.POISONS.2.NAME", EquipmentType.POISON, 
      [new LabelParam(58), new LabelParam(3), new LabelParam(51, {n: 20}), new LabelParam(61, {n: 0})], "EQUIPMENTS.POISONS.2.DESCRIPTION");
    this.addEquipment("EQUIPMENTS.POISONS.3.NAME", EquipmentType.POISON, 
      [new LabelParam(58), new LabelParam(79), new LabelParam(51, {n: 10}), new LabelParam(61, {n: 0})], "EQUIPMENTS.POISONS.3.DESCRIPTION");

    /* Services */
    this.addEquipment("EQUIPMENTS.SERVICES.0.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, null, null, [new CustomParam("14"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.SERVICES.1.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, null, null, [new CustomParam("30"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.SERVICES.2.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, null, null, [new CustomParam("43"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.SERVICES.3.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 10})]);
    this.addEquipment("EQUIPMENTS.SERVICES.4.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 30})]);
    this.addEquipment("EQUIPMENTS.SERVICES.5.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, null, null, [new CustomParam("ITEM_PRICE", true), new CustomParam("+"), new CustomParam("50")])]);
    this.addEquipment("EQUIPMENTS.SERVICES.6.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, null, null, [new CustomParam("20"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.SERVICES.7.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, null, null, [new CustomParam("18"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.SERVICES.8.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 20})]);
    this.addEquipment("EQUIPMENTS.SERVICES.9.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 54})]);
    this.addEquipment("EQUIPMENTS.SERVICES.10.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 5})]);
    this.addEquipment("EQUIPMENTS.SERVICES.11.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 120})]);
    this.addEquipment("EQUIPMENTS.SERVICES.12.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 5})]);
    this.addEquipment("EQUIPMENTS.SERVICES.13.NAME", EquipmentType.SERVICE, 
      [new LabelParam(51, {n: 1})]);
    this.addEquipment("EQUIPMENTS.SERVICES.14.NAME", EquipmentType.SERVICE, 
      [new LabelParam(83, {n: 25})]);

    /* Meals */
    this.addEquipment("EQUIPMENTS.MEALS.0.NAME", EquipmentType.MEAL, 
      [new LabelParam(51, {n: 1})]);
    this.addEquipment("EQUIPMENTS.MEALS.1.NAME", EquipmentType.MEAL, 
      [new LabelParam(51, {n: 1})]);
    this.addEquipment("EQUIPMENTS.MEALS.2.NAME", EquipmentType.MEAL, 
      [new LabelParam(51, {n: 15}, "PER_PERSON")]);

    /* Transportations */
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.0.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 50}), new LabelParam(82, {n: 20})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.1.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 75}), new LabelParam(82, {n: 10})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.2.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 400}), new LabelParam(82, {n: 12})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.3.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 150}), new LabelParam(82, {n: 40})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.4.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 50}), new LabelParam(82, {n: 15})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.5.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 150}), new LabelParam(82, {n: 20})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.6.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 5000}), new LabelParam(82, {n: 200})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.7.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 20000}), new LabelParam(82, {n: 100})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.8.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 1})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.9.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 10})]);
    this.addEquipment("EQUIPMENTS.TRANSPORTATION.10.NAME", EquipmentType.TRANSPORTATION, 
      [new LabelParam(51, {n: 100})]);

    /* Lands */
    this.addEquipment("EQUIPMENTS.LANDS.0.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 20})]);
    this.addEquipment("EQUIPMENTS.LANDS.1.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 500})]);
    this.addEquipment("EQUIPMENTS.LANDS.2.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 2500})]);
    this.addEquipment("EQUIPMENTS.LANDS.3.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 50000})]);
    this.addEquipment("EQUIPMENTS.LANDS.4.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 75000})]);
    this.addEquipment("EQUIPMENTS.LANDS.5.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 250000})]);
    this.addEquipment("EQUIPMENTS.LANDS.6.NAME", EquipmentType.LAND, 
      [new LabelParam(51, {n: 1000000})]);
    this.addEquipment("EQUIPMENTS.LANDS.7.NAME", EquipmentType.LAND, 
      [new LabelParam(83, {n: 1})]);

    /* Bribes */
    this.addEquipment("EQUIPMENTS.BRIBERY.0.NAME", EquipmentType.BRIBERY, 
      [new LabelParam(51, null, null, [new CustomParam("20"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.BRIBERY.1.NAME", EquipmentType.BRIBERY, 
      [new LabelParam(51, null, null, [new CustomParam("100"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.BRIBERY.2.NAME", EquipmentType.BRIBERY, 
      [new LabelParam(51, null, null, [new CustomParam("50"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.BRIBERY.3.NAME", EquipmentType.BRIBERY, 
      [new LabelParam(51, null, null, [new CustomParam("80"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    this.addEquipment("EQUIPMENTS.BRIBERY.4.NAME", EquipmentType.BRIBERY, 
      [new LabelParam(51, null, null, [new CustomParam("500"), new CustomParam("-"), new CustomParam("CHARISMA", true)])]);
    
    /* Gifts */
    this.addEquipment("EQUIPMENTS.GIFTS.0.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: 1})]);
    this.addEquipment("EQUIPMENTS.GIFTS.1.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: 55})]);    
    this.addEquipment("EQUIPMENTS.GIFTS.2.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: 200})]);    
    this.addEquipment("EQUIPMENTS.GIFTS.3.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: 75})]);    
    this.addEquipment("EQUIPMENTS.GIFTS.4.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: 105})]);    
    this.addEquipment("EQUIPMENTS.GIFTS.5.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: "350+"})]);
    this.addEquipment("EQUIPMENTS.GIFTS.6.NAME", EquipmentType.GIFT, 
      [new LabelParam(51, {n: 5000})]);

    /* Treasures */
    this.addEquipment("EQUIPMENTS.TREASURES.0.NAME", EquipmentType.TREASURE, 
      [new LabelParam(51, {n: 2})]);    
    this.addEquipment("EQUIPMENTS.TREASURES.1.NAME", EquipmentType.TREASURE, 
      [new LabelParam(51, {n: 5})]);    
    this.addEquipment("EQUIPMENTS.TREASURES.2.NAME", EquipmentType.TREASURE, 
      [new LabelParam(51, {n: 80})]);    
    this.addEquipment("EQUIPMENTS.TREASURES.3.NAME", EquipmentType.TREASURE, 
      [new LabelParam(51, {n: 250})]);    
    this.addEquipment("EQUIPMENTS.TREASURES.4.NAME", EquipmentType.TREASURE, 
      [new LabelParam(51, {n: 130000})]);           


  }
}
