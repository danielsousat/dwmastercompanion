import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Move } from '../dto/move';
import { Order } from '../dto/order';
import { Util } from '../util/util';
import { TranslateUtil } from './translate-util';

/*
  Generated class for the MovesService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MovesService {

  private translatedBasicMoves: Array<Move>;
  private translatedSpecialMoves: Array<Move>;
  private translatedGMMoves: Array<Move>;
  private translatedDungeonMoves: Array<Move>;

  constructor(public http: Http, public translateUtil: TranslateUtil) {
    console.log('Hello MovesService Provider');

    this.initMoves();
  }

  private initMoves() {

    this.translatedBasicMoves = this.translateAndSortMoves(MovesService.basicMoves);
    this.translatedSpecialMoves = this.translateAndSortMoves(MovesService.specialMoves);
    this.translatedGMMoves = this.translateAndSortMoves(MovesService.gmMoves);
    this.translatedDungeonMoves = this.translateAndSortMoves(MovesService.dungeonMoves);
  }

  private translateAndSortMoves(moves: Array<Move>): Array<Move> {

    return this.sortMoves(this.translateMoves(moves));
  }

  private translateMoves(moves: Array<Move>): Array<Move> {

    let list: Array<Move> = new Array();

    for (let move of moves) {

      let newMove: Move = new Move(this.translateUtil.translate(move.header));

      if (move.description != null) {
        newMove.description = this.translateUtil.translate(move.description);
      }

      list.push(newMove);
    }

    return list;
  }

  sortMoves(moves: Array<Move>, order: Order = Order.ASC): Array<Move> {

    return moves.sort((a, b) => Util.compareStrings(a.header, b.header, order));
  }

  getBasicMoves(): Array<Move> {

    return this.translatedBasicMoves;
  }

  getSpecialMoves(): Array<Move> {

    return this.translatedSpecialMoves;
  }

  getGMMoves(): Array<Move> {

    return this.translatedGMMoves;
  }

  getDungeonMoves(): Array<Move> {

    return this.translatedDungeonMoves;
  }

  getMovesTypes(): string[] {

    return Move.types;
  }

  private static basicMoves: Array<Move> = [
    new Move("MOVES.BASIC.0.HEADER", "MOVES.BASIC.0.DESCRIPTION"),
    new Move("MOVES.BASIC.1.HEADER", "MOVES.BASIC.1.DESCRIPTION"),
    new Move("MOVES.BASIC.2.HEADER", "MOVES.BASIC.2.DESCRIPTION"),
    new Move("MOVES.BASIC.3.HEADER", "MOVES.BASIC.3.DESCRIPTION"),
    new Move("MOVES.BASIC.4.HEADER", "MOVES.BASIC.4.DESCRIPTION"),
    new Move("MOVES.BASIC.5.HEADER", "MOVES.BASIC.5.DESCRIPTION"),
    new Move("MOVES.BASIC.6.HEADER", "MOVES.BASIC.6.DESCRIPTION"),
    new Move("MOVES.BASIC.7.HEADER", "MOVES.BASIC.7.DESCRIPTION")
  ];

  private static specialMoves: Array<Move> = [
    new Move("MOVES.SPECIAL.0.HEADER", "MOVES.SPECIAL.0.DESCRIPTION"),
    new Move("MOVES.SPECIAL.1.HEADER", "MOVES.SPECIAL.1.DESCRIPTION"),
    new Move("MOVES.SPECIAL.2.HEADER", "MOVES.SPECIAL.2.DESCRIPTION"),
    new Move("MOVES.SPECIAL.3.HEADER", "MOVES.SPECIAL.3.DESCRIPTION"),
    new Move("MOVES.SPECIAL.4.HEADER", "MOVES.SPECIAL.4.DESCRIPTION"),
    new Move("MOVES.SPECIAL.5.HEADER", "MOVES.SPECIAL.5.DESCRIPTION"),
    new Move("MOVES.SPECIAL.6.HEADER", "MOVES.SPECIAL.6.DESCRIPTION"),
    new Move("MOVES.SPECIAL.7.HEADER", "MOVES.SPECIAL.7.DESCRIPTION"),
    new Move("MOVES.SPECIAL.8.HEADER", "MOVES.SPECIAL.8.DESCRIPTION"),
    new Move("MOVES.SPECIAL.9.HEADER", "MOVES.SPECIAL.9.DESCRIPTION"),
    new Move("MOVES.SPECIAL.10.HEADER", "MOVES.SPECIAL.10.DESCRIPTION"),
    new Move("MOVES.SPECIAL.11.HEADER", "MOVES.SPECIAL.11.DESCRIPTION"),
    new Move("MOVES.SPECIAL.12.HEADER", "MOVES.SPECIAL.12.DESCRIPTION"),
    new Move("MOVES.SPECIAL.13.HEADER", "MOVES.SPECIAL.13.DESCRIPTION")
  ];

  private static gmMoves: Array<Move> = [
    new Move("MOVES.GM.0"),
    new Move("MOVES.GM.1"),
    new Move("MOVES.GM.2"),
    new Move("MOVES.GM.3"),
    new Move("MOVES.GM.4"),
    new Move("MOVES.GM.5"),
    new Move("MOVES.GM.6"),
    new Move("MOVES.GM.7"),
    new Move("MOVES.GM.8"),
    new Move("MOVES.GM.9"),
    new Move("MOVES.GM.10"),
    new Move("MOVES.GM.11")
  ];

  private static dungeonMoves: Array<Move> = [
    new Move("MOVES.DUNGEON.0"),
    new Move("MOVES.DUNGEON.1"),
    new Move("MOVES.DUNGEON.2"),
    new Move("MOVES.DUNGEON.3"),
    new Move("MOVES.DUNGEON.4"),
    new Move("MOVES.DUNGEON.5"),
    new Move("MOVES.DUNGEON.6")
  ];

}
