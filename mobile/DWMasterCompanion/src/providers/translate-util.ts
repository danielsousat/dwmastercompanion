import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from 'ng2-translate/ng2-translate';
import 'rxjs/add/operator/map';
import { Util } from '../util/util';
import { TranslatableError } from '../exception/translatable-error';

/*
  Generated class for the TranslateUtil provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TranslateUtil {

  constructor(public http: Http, public translateService: TranslateService) {
    console.log('Hello TranslateUtil Provider');
  }

  translate(code: string, params?: Object): string {

    let res: string;

    this.translateService.get(code, params).subscribe((value) => res = value);
    
    return res;
  }

  getRandomArrayItemTranslated(array: string[]): string {
    let code: string = Util.getArrayRandomItem(array);

    return this.translate(code);
  }

  getTranslatedErrorMessage(error: TranslatableError): string {

    return this.translate(error.messageCode, error.messageParams);
  }
}
