import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateUtil } from './translate-util';
import 'rxjs/add/operator/map';
import { Place } from '../dto/place';

/*
  Generated class for the PlacesGeneratorService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PlacesGeneratorService {

  lastGeneratedPlace: Place = null;

  
  constructor(public http: Http, public translateUtil: TranslateUtil) {
    console.log('Hello PlacesGeneratorService Provider');

  }

  generateNewPlace(): Place {

    let newPlace: Place = new Place();
    
    newPlace.type = this.translateUtil.getRandomArrayItemTranslated(Place.types);
    newPlace.adjective = this.translateUtil.getRandomArrayItemTranslated(Place.adjectives);
    newPlace.firstCharacteristic = this.translateUtil.getRandomArrayItemTranslated(Place.firstCharacteristics);
    newPlace.secondCharacteristic = this.translateUtil.getRandomArrayItemTranslated(Place.secondCharacteristics);

    this.lastGeneratedPlace = newPlace;

    return newPlace;

  }

}
