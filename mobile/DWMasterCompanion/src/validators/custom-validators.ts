import { AbstractControl, ValidatorFn } from '@angular/forms';

export abstract class CustomValidators {

    static maxValue(max: number): ValidatorFn {

        return (control: AbstractControl): {[key: string]: any} => {

            let value: number = control.value;
            
            let ret = CustomValidators.maxValueCheck(value, max) ? null : {'number': {value}}; 
            
            return ret;
        }
    }

    static minValue(min: number): ValidatorFn {

        return (control: AbstractControl): {[key: string]: any} => {

            let value: number = control.value;

            /* returns null if validation ok */
            return CustomValidators.minValueCheck(value, min) ? null : {'number': {value}};
        }
    }

    static maxValueCheck(value: number, max: number): boolean {

        return value <= max;
    }

    static minValueCheck(value: number, min: number): boolean {

        return value >= min;
    }

}